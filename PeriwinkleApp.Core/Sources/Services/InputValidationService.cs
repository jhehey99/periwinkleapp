﻿using System.Linq;
using System.Text.RegularExpressions;
using Java.Lang;
using PeriwinkleApp.Core.Sources.Exceptions;
using PeriwinkleApp.Core.Sources.Services.Interfaces;
using String = System.String;

namespace PeriwinkleApp.Core.Sources.Services
{
    public class InputValidationService : IInputValidationService
    {
    #region Public Methods

        // chars min 5, max 32, is a Word
        public void ValidateUsername (string username, int min = 5, int max = 32)
        {
            string pattern = @"^(\w{" + min + "," + max + "})$";

            var ex = Match (pattern, username, "Invalid Username and length within 5-32 chars");
            if (ex != null) throw ex;
        }
        
        // chars min 5, max 32, Contains at least 1 digit, 1 Uppercase, is a Word
        public void ValidatePassword (string password, int min = 5, int max = 32)
        {
            string pattern = @"(?=.*[\d])(?=.*[A-Z])^(\w{" + min + "," + max + "})$";

            var ex = Match (pattern, password, "Invalid Password and must contain 1 Uppercase and 1 Digit");
            if (ex != null) throw ex;
        }
        
        // word, min 1, max 64
        public void ValidateFirstName (string fname, int min = 1, int max = 64)
        {
            string pattern = @"^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ ,.'_-]{"+
                        min + "," + max + "}$";

            var ex = Match (pattern, fname, "Invalid First Name and length within 1-64 chars");
            if (ex != null) throw ex;
        }

        // word, min 1, max 48
        public void ValidateLastName (string lname, int min = 1, int max = 48)
        {
            string pattern = @"^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ ,.'_-]{"+
                             min + "," + max + "}$";

            var ex = Match (pattern, lname, "Invalid Last Name and length within 1-48 chars");
            if (ex != null) throw ex;
        }

        // Digits, start with 09 + 9 digits
        public void ValidateContact (string contact)
        {
            const string pattern = @"^(09[\d]{9,9})$";
            
            var ex = Match (pattern, contact, "Invalid Contact Number, must start with 09. ex.(09052323506)");
            if (ex != null) throw ex;
        }
        
        // 128 max, may @ then .
        public void ValidateEmail (string email)
        {
            const string pattern = @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Za-z0-9.-]+.[A-Za-z0-9]{2,}$";
            
            if(String.IsNullOrEmpty (email) || email.Length > 128)
                throw new InputValidationException ("Invalid Email Address");
            
            var ex = Match (pattern, email, "Invalid Email Address");
            if (ex != null) throw ex;
        }

    #endregion

    #region Private Methods

        private PeriwinkleException Match (string pattern, string input, string errMsg)
        {
            Regex regex = new Regex (pattern);

            // Match = no exception, No Match = exception
            return regex.IsMatch (input) ? null : new InputValidationException (errMsg);
        }
        
    #endregion

    }
}
