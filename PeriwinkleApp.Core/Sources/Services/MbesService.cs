﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PeriwinkleApp.Core.Sources.Extensions;
using PeriwinkleApp.Core.Sources.Models.Domain;
using PeriwinkleApp.Core.Sources.Models.Response;
using PeriwinkleApp.Core.Sources.Services.Interfaces;

namespace PeriwinkleApp.Core.Sources.Services
{
    public class MbesService : PeriwinkleHttpService, IMbesService
    {
        private new static string Tag => "MbesService";

		public async Task <List <ApiResponse>> AddClientMbesResponse (MbesResponse mbesResponse)
		{
			string url = ApiUri.AddClientMbesResponse.ToUrl ();

			var response = await httpService.PostReadResponse
							   <IEnumerable<ApiResponse>, MbesResponse>(url, mbesResponse);

			return response.ToList();
		}
	}
}
