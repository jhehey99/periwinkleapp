﻿namespace PeriwinkleApp.Core.Sources.Models.Common
{
	public class ChartStat
	{
		// all in string so display nlng problem
		public string StartTime { get; set; }
		public string StopTime { get; set; }
		public string Duration { get; set; }
		public string MaxValue { get; set; }
		public string MinValue { get; set; }
		public string AveValue { get; set; }
	}
}
