﻿namespace PeriwinkleApp.Core.Sources.Models.Response
{
    public enum ApiResponseCode
    {
        UsernameTaken = 910,
        EmailTaken = 911,
        ContactTaken = 912,
        LicenseTaken = 913,
        RegisterFailed = 980,
        UpdateFailed = 981,
        LoginFailed = 982,
        RegisterSuccess = 990,
        UpdateSuccess = 991
    }
}
