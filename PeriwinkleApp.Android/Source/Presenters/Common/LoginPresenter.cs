﻿using PeriwinkleApp.Android.Source.Cache;
using PeriwinkleApp.Android.Source.Factories;
using PeriwinkleApp.Android.Source.Session;
using PeriwinkleApp.Android.Source.Views.Fragments.Common;
using PeriwinkleApp.Core.Sources.Models.Domain;
using PeriwinkleApp.Core.Sources.Services;
using PeriwinkleApp.Core.Sources.Services.Interfaces;
using PeriwinkleApp.Core.Sources.Utils;

namespace PeriwinkleApp.Android.Source.Presenters.Common
{
    public interface ILoginPresenter
    {
        void PerformLogin (string username, string password);
    }

    public class LoginPresenter : ILoginPresenter
    {
        private readonly ILoginView view;
        private readonly IHashService hashService;
        private readonly IAccountService accService;
        private readonly IPasswordService passService;

        private AccountSession loginSession;

        //LoginSession

        public LoginPresenter (ILoginView view)
        {
            this.view = view;
            hashService = hashService ?? new HashService();
            accService = accService ?? new AccountService ();
            passService = passService ?? new PasswordService();
        }

        public async void PerformLogin (string username, string password)
        {
            // Empty ung input so error
            if (username == "" || password == "")
            {
                view.DisplayLoginError (true);
                return;
            }
            
            // TODO Kung sakali, strip ung special

            // Get Account Session details
            Account loggedAccount = await accService.GetAccountAsSession (username);

            if (loggedAccount == null)
            {
                view.DisplayLoginError (true);
                return;
            }

            // username exists, check na natin kung tama ung password
            view.DisplayLoginError (false);

            // verify natin ung password na input at ni account
            Password userPassword = await passService.GetPasswordByUsername (username);
            bool validPass = hashService.VerifyPasswordHash (password, userPassword);

            // pag di valid, show tayo ng error
            if (!validPass)
            {
                view.DisplayLoginError(true);
                return;
            }

            // create ung session sa nag log in na account
            loginSession = SessionFactory.CreateSession<AccountSession>(SessionKeys.LoginKey);
            loginSession.AddAccountSession(loggedAccount);

//			//TODO CACHE
//			CacheProvider.Set<Account> (SessionKeys.LoginKey, loggedAccount);

            // user exists and valid na ung password, so login na given ung account type for activity
            if (loggedAccount.AccTypeId != null)
                view.UserLoginSuccess ((AccountType) loggedAccount.AccTypeId);

            //TODO TANGAL MO KO
            Logger.Debug (loggedAccount);
        }
    }
}
