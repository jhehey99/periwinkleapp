﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using PeriwinkleApp.Android.Source.Services;

namespace PeriwinkleApp.Android.Source.Views.Activities
{
	[Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class TestActivity : AppCompatActivity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			AndroidFileService afService = new AndroidFileService ();

			afService.SaveFile ();

		}
	}
}
