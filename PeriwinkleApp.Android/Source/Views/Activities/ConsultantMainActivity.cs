﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using PeriwinkleApp.Android.Source.Dictionaries;
using PeriwinkleApp.Android.Source.Presenters.ConsultantPresenters;
using PeriwinkleApp.Core.Sources.Models.Domain;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using v4App = Android.Support.V4.App;

namespace PeriwinkleApp.Android.Source.Views.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConsultantMainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener, IConsultantMainActivity
    {
        TextView txtNavHeaderName, txtNavHeaderEmail;

        private IConsultantMainPresenter presenter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_consultant_main);

            // Toolbar
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            // Float Action Button
//            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
//            fab.Click += FabOnClick;

            // Drawer Layout
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.consul_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            // Navigation View
            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.consul_nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            // Nav Header View and Header Text Views
            View headerView = navigationView.GetHeaderView(0);
            txtNavHeaderName = headerView.FindViewById<TextView>(Resource.Id.txt_navHeader_name);
            txtNavHeaderEmail = headerView.FindViewById<TextView>(Resource.Id.txt_navHeader_email);

            // Access ung Login Session
            // TODO Mula sa presenter nito, kukunin natin ung session
            presenter = new ConsultantMainPresenter (this);

            // Set the default navigation drawer item upon startup
            IMenuItem item = navigationView.Menu.FindItem(Resource.Id.con_menu_home);
            navigationView.SetCheckedItem(Resource.Id.con_menu_home);
            OnNavigationItemSelected(item);

            Window.SetSoftInputMode(SoftInput.StateHidden);
        }

//        protected override async void OnPostCreate (Bundle savedInstanceState)
//        {
//            base.OnPostCreate (savedInstanceState);
//            await presenter.LoadNavHeaderDetails ();
//        }
        
        public void SetNavHeaderDetails(string name, string email)
        {
			// TODO: DETERMINE KUNG REGISTERED CONSULTANT NA 
			// TODO: OR PENDING PALANG. PAG PENDING PALANG MAG LAGAY NALANG TAYO NG DISPLAY
			// TODO: INDICATING NA PENDING SYA AND WALA PA SYA KAYANG GAWIN AND WAIT FOR EMAIL
            txtNavHeaderName.Text = name;
            txtNavHeaderEmail.Text = email;
        }

		public void LaunchPendingConsultantActivity ()
		{
			throw new NotImplementedException ();
		}

		public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.consul_drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                int count = SupportFragmentManager.BackStackEntryCount;

                if (count > 0)
                    SupportFragmentManager.PopBackStack();
                else
                    base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            return id == Resource.Id.action_settings || base.OnOptionsItemSelected(item);
        }

        private void FabOnClick (object sender, EventArgs e)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                    .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            int id = menuItem.ItemId;

            v4App.FragmentTransaction ft = SupportFragmentManager.BeginTransaction();

            NavigationItemMap navMap = new NavigationItemMap(AccountType.Consultant);

            (int? titleId, v4App.Fragment fragment) = navMap.GetNavItem(id);

            if (titleId != null)
                SupportActionBar.SetTitle((int)titleId);

            if (fragment != null)
            {
                ft.Replace(Resource.Id.fragment_container, fragment);
//                ft.AddToBackStack(null);
                ft.Commit();
            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.consul_drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
            imm.HideSoftInputFromWindow(Window.DecorView.WindowToken, HideSoftInputFlags.NotAlways);
            return base.OnTouchEvent(e);
        }
        
        
    }
}
