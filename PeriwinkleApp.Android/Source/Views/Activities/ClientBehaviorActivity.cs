﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Microcharts;
using Microcharts.Droid;
using PeriwinkleApp.Android.Source.Presenters.ClientPresenters;

namespace PeriwinkleApp.Android.Source.Views.Activities
{
	public interface IClientBehaviorActivity
	{
		void DisplayLineChart(LineChart lineChart);
		void SetStartTime (string startTime);
		void SetStopTime (string stopTime);
		void CreateRandomEntryOnUiThread ();
	}


	[Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ClientBehaviorActivity : AppCompatActivity, IClientBehaviorActivity
	{
		private TextView txtStartTimeVal, txtStopTimeVal;
		private Button btnStartTime, btnStopTime;

        private ChartView chartView;

		private IClientBehaviorPresenter presenter;
		
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			RequestedOrientation = ScreenOrientation.Landscape;
			SetContentView (Resource.Layout.client_frag_behavior);

			// set reference sa chartview
			chartView = FindViewById <ChartView> (Resource.Id.chart_view_behavior);

            // time text views
			txtStartTimeVal = FindViewById<TextView>(Resource.Id.txt_bhv_start_time_val);
			txtStopTimeVal = FindViewById<TextView>(Resource.Id.txt_bhv_stop_time_val);

            // time button
			btnStartTime = FindViewById <Button> (Resource.Id.btn_bhv_start_time);
			btnStopTime = FindViewById <Button> (Resource.Id.btn_bhv_stop_time);

			// button click events
			btnStartTime.Click += OnStartTimeClicked;
			btnStopTime.Click += OnStopTimeClicked;

			// initialize presenter
            presenter = new ClientBehaviorPresenter (this);
//			presenter.LoadInitialLineChartData ();
		}

		private void OnStartTimeClicked(object sender, EventArgs e)
		{
			presenter.StartTimer ();
		}

        private void OnStopTimeClicked (object sender, EventArgs e)
		{
			presenter.StopTimer ();
		}



        #region IClientBehaviorActivity

		private LineChart chart;
        public void DisplayLineChart(LineChart lineChart)
		{
			this.chart = lineChart;
			RunOnUiThread (UpdateLineChart);
		}

		private void UpdateLineChart ()
		{
			chartView.Chart = chart;
        }

        public void SetStartTime (string startTime)
		{
			txtStartTimeVal.Text = startTime;
		}

        public void SetStopTime (string stopTime)
		{
			txtStopTimeVal.Text = stopTime;
		}

		public void CreateRandomEntryOnUiThread ()
		{
			// Kailangan kasi i-update ung ui on the ui thread.
			// ung timer kasi, is nagru-run on a separate thread.
			RunOnUiThread (() => { presenter.CreateRandomEntry (); });
		}

#endregion


        public override void OnBackPressed()
		{
			base.OnBackPressed();
			Intent intent = new Intent (ApplicationContext, typeof (ClientMainActivityTest));
			StartActivity (intent);
			Finish();
        }


    }
}
