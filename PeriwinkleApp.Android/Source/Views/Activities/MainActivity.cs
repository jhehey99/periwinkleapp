﻿using System;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using PeriwinkleApp.Android.Source.Dictionaries;
using Widget = Android.Widget;
using v4App = Android.Support.V4.App;
using PeriwinkleApp.Core.Sources.Models.Domain;

namespace PeriwinkleApp.Android.Source.Views.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        private Widget.TextView txtNavHeaderName, txtNavHeaderEmail;

//        private LoginSession accountSession;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            // Toolbar
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            
            // Float Action Button
//            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
//            fab.Click += FabOnClick;

            // Drawer Layout
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            // Navigation View
            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            // Nav Header View and Header Text Views
            View headerView = navigationView.GetHeaderView(0);
            txtNavHeaderName = headerView.FindViewById<Widget.TextView>(Resource.Id.txt_navHeader_name);
            txtNavHeaderEmail = headerView.FindViewById<Widget.TextView>(Resource.Id.txt_navHeader_email);
            
            txtNavHeaderName.Text = "Jhon Paul Jaspe";
            txtNavHeaderEmail.Text = "jhonpauljaspe99@gmail.com";

            // SESSION TODO UNG CREATE SESSION NA MULA SA FACTORY
//            string username = "admin";
//            AccountType type = AccountType.Admin;
//            accountSession = new LoginSession(ApplicationContext);
//            accountSession.Login(username, type);
        }
        
        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if(drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                int count = SupportFragmentManager.BackStackEntryCount;

                if (count > 0)
                    SupportFragmentManager.PopBackStack();
                else
                    base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            return id == Resource.Id.action_settings || base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            int id = menuItem.ItemId;
           
            v4App.FragmentTransaction ft = SupportFragmentManager.BeginTransaction();

            NavigationItemMap navMap = new NavigationItemMap(AccountType.Admin);

            (int? titleId, v4App.Fragment fragment) = navMap.GetNavItem(id);

             if (titleId != null)
                SupportActionBar.SetTitle((int)titleId);

            if (fragment != null)
            {
                ft.Replace(Resource.Id.fragment_container, fragment);
                ft.AddToBackStack(null);
                ft.Commit();
            }
            
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }


    }
}

