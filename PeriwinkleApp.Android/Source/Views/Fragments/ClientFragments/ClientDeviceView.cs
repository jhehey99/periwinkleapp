﻿using System;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using PeriwinkleApp.Core.Sources.Utils;

namespace PeriwinkleApp.Android.Source.Views.Fragments.ClientFragments
{
    public class ClientDeviceView : Fragment
	{

		private Switch btSwitch;
		private Button btnDiscover;
		private BluetoothAdapter btAdapter;

		const int REQUEST_ENABLE_BT = 3;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.client_frag_device, container, false);
        }

		public override void OnViewCreated (View view, Bundle savedInstanceState)
		{
			base.OnViewCreated (view, savedInstanceState);

			// Bluetooth On/Off Switch
			btSwitch = view.FindViewById <Switch> (Resource.Id.swt_cli_dev_bt);
			btSwitch.CheckedChange += OnBluetoothSwitchChanged;

			// Bluetooth Discover Devices Button
			btnDiscover = view.FindViewById <Button> (Resource.Id.btn_cli_dev_discover);
			btnDiscover.Click += OnBluetoothDiscoverClicked;

			// Get default bluetooth adapter
			btAdapter = BluetoothAdapter.DefaultAdapter;

		}

		private void OnBluetoothSwitchChanged (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			if (e.IsChecked)
				TurnOnBluetooth ();
			else
				TurnOffBluetooth ();
		}

		private void OnBluetoothDiscoverClicked(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		private void TurnOnBluetooth ()
		{
			btSwitch.Text = "Turn Bluetooth Off";
			if (btAdapter == null)
			{
				// message tayo dito device doesn't support bluetooth
				Logger.Log ("Device doesn't support Bluetooth");
				return;
			}
			
			// not enabled, so we turn it on
			if (!btAdapter.IsEnabled)
			{
				Intent enableIntent = new Intent (BluetoothAdapter.ActionRequestEnable);
				StartActivityForResult (enableIntent, REQUEST_ENABLE_BT);
			}


        }

        private void TurnOffBluetooth ()
		{
			btSwitch.Text = "Turn Bluetooth On";

			if (btAdapter == null)
			{
				// message tayo dito device doesn't support bluetooth
				Logger.Log("Device doesn't support Bluetooth");
				return;
			}

			// enabled, so we turn it off
			if (btAdapter.IsEnabled)
			{
				btAdapter.Disable ();
				Intent enableIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
				StartActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			}
        }
    }
}
