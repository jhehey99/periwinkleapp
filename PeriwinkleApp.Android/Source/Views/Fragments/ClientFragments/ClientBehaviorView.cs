﻿using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using PeriwinkleApp.Android.Source.Views.Activities;

namespace PeriwinkleApp.Android.Source.Views.Fragments.ClientFragments
{
    public class ClientBehaviorView : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			//TODO TANGAL MO KO
            // Create your fragment here
			int CliBehActRequestCode = 2;
			Intent intent = new Intent(Context, typeof(ClientBehaviorActivity));
			StartActivityForResult (intent, CliBehActRequestCode);
//			Activity.RequestedOrientation = ScreenOrientation.Landscape;
        }

		public override void OnActivityResult (int requestCode, int resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			Activity.OnBackPressed ();
		}

//
//        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
//        {
//            // Use this to return your custom view for this Fragment
//            return inflater.Inflate(Resource.Layout.client_frag_behavior, container, false);
//        }
	}
}
