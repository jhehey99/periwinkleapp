﻿using System.Collections.Generic;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using PeriwinkleApp.Android.Source.AdapterModels;
using PeriwinkleApp.Android.Source.Adapters;
using PeriwinkleApp.Android.Source.Factories;
using PeriwinkleApp.Android.Source.Presenters.ConsultantPresenters;

namespace PeriwinkleApp.Android.Source.Views.Fragments.ConsultantFragments
{
  
    public class ConsultantClientsView : Fragment, IConsultantClientsView
    {
        private LinearLayout linearLayout;

        private ProgressBar progressBar;

        private RecyclerView recyclerView;

        private RecyclerView.LayoutManager layoutManager;

        private AccountRecyclerAdapter recyclerAdapter;

        private IConsultantClientsPresenter presenter;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            presenter = new ConsultantClientsPresenter (this);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.list_frag_generic, container, false);
        }

        public override async void OnViewCreated (View view, Bundle savedInstanceState)
        {
            base.OnViewCreated (view, savedInstanceState);

            // Start Progressbar
            progressBar = view.RootView.FindViewById<ProgressBar>(Resource.Id.list_frag_gen_progress);
            progressBar.Visibility = ViewStates.Visible;

            // Linear Layout
            linearLayout = view.RootView.FindViewById<LinearLayout>(Resource.Id.list_frag_gen_linear);
            linearLayout.SetGravity(GravityFlags.Center);

            // RecyclerView
            recyclerView = view.RootView.FindViewById<RecyclerView>(Resource.Id.list_frag_gen_recyclerView);

            // Layout Manager
            layoutManager = new LinearLayoutManager(view.Context);
            recyclerView.SetLayoutManager(layoutManager);

            // Adapter
            recyclerAdapter = new AccountRecyclerAdapter();
            recyclerAdapter.ItemClick += OnItemClick;

            var animationAdapter = AnimationFactory.CreateRecyclerAnimation(recyclerAdapter);
            var animator = AnimationFactory.CreateRecyclerAnimator();

            recyclerView.SetAdapter(animationAdapter);
            recyclerView.SetItemAnimator(animator);

            // eto ung pagkuha ng clients' list
            await presenter.GetAllMyClients ();
        }

        private void OnItemClick (object sender, int position)
        {
            int photoNum = position + 1;
            Toast.MakeText(this.Activity, "This is photo number " + photoNum, ToastLength.Short).Show();
        }

        public void EndProgressBar()
        {
            progressBar.Visibility = ViewStates.Gone;
            linearLayout.SetGravity(GravityFlags.Center | GravityFlags.Top);
        }

        #region IConsultantClientsView

        public void DisplayMyClientsList (List <AccountAdapterModel> accountDataSet)
        {
            if(accountDataSet != null)
                recyclerAdapter.UpdateList (accountDataSet);

            EndProgressBar ();
        }

		public void LaunchClientProfile ()
		{
			throw new System.NotImplementedException ();
		}

#endregion
    }
}
