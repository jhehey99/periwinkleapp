﻿using System;
using Android.Bluetooth;
using Java.IO;
using Java.Lang;
using PeriwinkleApp.Core.Sources.Utils;
using Console = System.Console;
using Exception = Java.Lang.Exception;

namespace PeriwinkleApp.Android.Source.Services.Bluetooth
{
	public class ConnectThread : Thread
	{
		private readonly BluetoothService service;
		private readonly BluetoothSocket socket;
		private BluetoothAdapter BtAdapter;
        private BluetoothDevice device;


        public ConnectThread (BluetoothService service, BluetoothDevice device)
		{
			this.service = service;
			this.device = device;
			BtAdapter = this.service.BtAdapter;

            BluetoothSocket tempSocket = null;
			try
			{
				tempSocket = device.CreateRfcommSocketToServiceRecord (service.BtUUID);
			}
			catch (IOException e)
			{
				Logger.Log("Socket's create() method failed " + e.Message);
			}

            socket = tempSocket;
		}

		public override void Run ()
		{
			BtAdapter.CancelDiscovery ();

			try
			{
				socket.Connect ();
			}
			catch (IOException e)
			{
				CloseSocket ();
				return;
			}

			service.ManageConnectedSocket (socket);
        }

		public void Cancel ()
		{
			CloseSocket ();
		}

		private void CloseSocket ()
		{
			try
			{
				socket.Close();
			}
			catch (IOException e)
			{
				Logger.Log("Could not close the client socket " + e.Message);
			}
        }
	}
}
