﻿using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Java.Lang;
using Java.Util;

namespace PeriwinkleApp.Android.Source.Services.Bluetooth
{
	public enum MessageConstants
	{
		Read,
		Write,
		Toast
	};

	public class BluetoothService
	{
		public BluetoothAdapter BtAdapter { get; private set; }
		public Handler MessageHandler { get; private set; }

        public string BtName { get; }
		public UUID BtUUID { get; }

		private Context context;

		private AcceptThread acceptThread;
		private ConnectThread connectThread;
		private ConnectedThread connectedThread;

		public BluetoothService (Context context, Handler handler)
		{
			this.context = context;
			MessageHandler = handler;
			BtAdapter = BluetoothAdapter.DefaultAdapter;
			Start ();
		}

		public void ManageConnectedSocket (BluetoothSocket socket)
		{
			connectedThread = new ConnectedThread (this, socket);
			connectThread.Start ();
		}

		public void Start ()
		{
			if (connectThread != null)
			{
				connectThread.Cancel ();
				connectThread = null;
			}

			if (acceptThread == null)
			{
				acceptThread = new AcceptThread (this);
				acceptThread.Start ();
			}
		}

		public void StartClient (BluetoothDevice device)
		{
			connectThread = new ConnectThread (this, device);
			connectThread.Start ();
		}

		public void Write (byte[] bytesToWrite)
		{
			connectedThread.Write (bytesToWrite);
		}
	}
}
