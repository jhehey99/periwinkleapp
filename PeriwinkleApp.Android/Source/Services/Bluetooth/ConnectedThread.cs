﻿using System;
using System.IO;
using Android.Bluetooth;
using Android.OS;
using Java.Lang;
using PeriwinkleApp.Core.Sources.Utils;
using Exception = Java.Lang.Exception;
using IOException = System.IO.IOException;

namespace PeriwinkleApp.Android.Source.Services.Bluetooth
{
	public class ConnectedThread : Thread
	{
		private readonly BluetoothService service;
        private BluetoothSocket socket;
		private Stream InStream;
		private Stream OutStream;
		private byte[] buffer;
		private Handler handler;

        public ConnectedThread (BluetoothService service, BluetoothSocket socket)
		{
			this.service = service;
			this.socket = socket;
			Stream tempIn = null;
			Stream tempOut = null;
			this.handler = this.service.MessageHandler;

            try
            {
				tempIn = this.socket.InputStream;
			}
			catch (IOException e)
			{
				Logger.Log("Error occurred when creating input stream " + e.Message);
            }

			try
			{
				tempOut = this.socket.OutputStream;
			}
			catch (IOException e)
			{
				Logger.Log("Error occurred when creating output  stream " + e.Message);
			}

            InStream = tempIn;
			OutStream = tempOut;
		}

		public override void Run ()
		{
			const int bufferSize = 1024;
			buffer = new byte[bufferSize];

			while (true)
			{
				try
				{
					// Read from the InputStream
					int numBytes = InStream.Read (buffer, 0, bufferSize);
					Message readMessage = handler.ObtainMessage ((int) MessageConstants.Read, numBytes, -1, buffer);
					readMessage.SendToTarget ();
				}
				catch (IOException e)
				{
					Logger.Log("Input stream was disconnected " + e.Message);
					break;
				}
            }
		}

		public void Write (byte[] bytesToWrite)
		{
			try
			{
				OutStream.Write (bytesToWrite, 0, bytesToWrite.Length);
				Message writtenMessage = handler.ObtainMessage ((int) MessageConstants.Write, -1, -1, bytesToWrite);
				writtenMessage.SendToTarget ();

			}
			catch (IOException e)
			{
				Logger.Log("Error occurred when sending data " + e.Message);

                Message writeErrorMessage = handler.ObtainMessage ((int) MessageConstants.Toast);
				Bundle bundle = new Bundle();
				bundle.PutString ("toast", "Couldn't send data to the other device");
				writeErrorMessage.Data = bundle;
				handler.SendMessage (writeErrorMessage);
			}
        }
		
		public void Cancel ()
		{
			try
			{
				socket.Close ();
			}
			catch (IOException e)
			{
				Logger.Log("Could not close the connect socket " + e.Message);
            }
        }
    }
}
